import { useEffect, useState } from "react";
import { useLocation } from "wouter";
import { useCartContext } from "../contexts/CartContext";
import { addToCart } from "../apis/cart";
import "./Products.css";

export type Product = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  rating: { rate: number; count: number };
  title: string;
};

export default function Products() {
  const [_, setLocation] = useLocation();
  const [products, setProducts] = useState<Product[]>([]);
  const { dispatch } = useCartContext();

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => setProducts(json));
  }, []);

  return (
    <>
      <div style={{ flexWrap: "wrap", gap: "8px" }} className="prods-container">
        {products.map((product) => (
          <div
            key={product.id}
            style={{ padding: "8px", backgroundColor: "white", width: "160px" }}
            className="product-container"
          >
            <img
              src={product.image}
              alt={product.title}
              style={{ width: "100%", maxHeight: "200px" }}
            />
            <p style={{ fontSize: ".8rem" }}>
              <strong>{product.title}</strong>
            </p>

            <div className="prod-btns">
                <button
                  style={{ width: "100%" }}
                  onClick={() => {
                    setLocation(`/products/${product.id}`);
                  }}
                  className="view-btn"
                >
                  View
                </button>
                <div style={{ margin: "2px" }} />
                <button
                  onClick={() => {
                    addToCart({
                      title: product.title,
                      productId: product.id,
                    }).then((cartItem) =>
                      dispatch({ type: "addToCart", cartItem })
                    );
                  }}
                  className="toCart-btn"
                >
                  Add to Cart
                </button>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
