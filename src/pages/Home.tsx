import { Link } from "wouter";
import "./Home.css";

export default function Home () {
  return(
    <div className="landingpage">
      <h2 className="welcome-message"><span className="max">Max</span><span className="sky">Sky</span></h2>
      <Link href="/products">
        <button className="to-products-btn">Check out our products</button>
      </Link>
    </div>
  )
}
