import { Link } from "wouter";
import { useCartContext } from "./contexts/CartContext";
import cartImg from "./images/cart1.png";
import "./CartCount.css";

export default function CartCount() {
  const { cart } = useCartContext();

  return (
    <>
    <div className="cartLogo">
      <Link href="/cart" className="cart-link">
        <img src={cartImg}
          style={{ height: "50px" }}
        />
        
        <p className="cartCount">{cart.length}</p>
        
        </Link>
    </div>
    </>
  );
}
